before_first_call
=================

.. contents::
  Table of contents
  :local:

.. flask_gordon/middleware/before_first_call.py
.. automodule:: flask_gordon.middleware.before_first_call
