dispatcher
==========

.. contents::
  Table of contents
  :local:

.. flask_gordon/middleware/dispatcher.py
.. automodule:: flask_gordon.middleware.dispatcher
