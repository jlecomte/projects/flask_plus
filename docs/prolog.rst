.. Environment variables


.. role:: param
  :class: param

.. role:: url
  :class: url

.. role:: ev
  :class: envvar

.. Word emphasis

.. role:: em
  :class: em-emphasis

.. Highlighted word emphasis

.. role:: hi
  :class: hi-emphasis

.. docker

.. role:: docker
  :class: docker

.. mailto

.. role:: mailto
  :class: mailto

.. Server

.. role:: server
  :class: server

.. User

.. role:: user
  :class: user

.. An uncheck checkmark

.. |uncheck| raw:: html

  <input type="checkbox">
