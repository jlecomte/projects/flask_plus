celery
======

.. contents::
  Table of contents
  :local:

.. flask_gordon/ext/celery/celery.py
.. automodule:: flask_gordon.ext.celery.celery

.. flask_gordon/ext/defaults.py
.. automodule:: flask_gordon.ext.defaults
  :no-index:
