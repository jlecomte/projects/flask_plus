[![license](https://img.shields.io/badge/license-MIT-brightgreen)](https://spdx.org/licenses/MIT.html)
[![documentation](https://img.shields.io/badge/documentation-html-informational)](https://cappysan.docs.gitlab.io/python/flask-gordon)
[![pipelines](https://gitlab.com/cappysan/flask-gordon/badges/master/pipeline.svg)](https://gitlab.com/cappysan/flask-gordon/pipelines)
[![coverage](https://gitlab.com/cappysan/flask-gordon/badges/master/coverage.svg)](https://flask-gordon.docs.cappysan.dev/coverage/index.html)

# Flask Gordon

Flask blueprint, route, and middleware extensions.

## Installation from PyPI

You can install the latest version from PyPI package repository.

~~~bash
python3 -mpip install -U flask-gordon
~~~


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://flask-gordon.docs.cappysan.dev](https://flask-gordon.docs.cappysan.dev)
  * Website: [https://gitlab.com/cappysan/flask-gordon](https://gitlab.com/cappysan/flask-gordon)
  * PyPi: [https://pypi.org/project/flask-gordon](https://pypi.org/project/flask-gordon)
