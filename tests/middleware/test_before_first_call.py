# pylint: disable=global-variable-not-assigned
# pylint: disable=global-statement
# pylint: disable=invalid-name
from flask import Flask

from flask_gordon.middleware import BeforeFirstCall

calls = 0


def dummy_once():
    global calls
    calls += 1


# pylint: disable=redefined-outer-name
def assert_call_count(app):
    global calls
    calls = 0

    client = app.test_client()

    # Call twice, expect only one call on before_first_call
    client.get("/")
    client.get("/")

    assert calls == 1, "before_first_call function called more than once"


def test_middleware_dispatcher():
    app = Flask(__name__)
    app.wsgi_app = BeforeFirstCall(app.wsgi_app, dummy_once)
    assert_call_count(app)


app = Flask(__name__)
bfc = BeforeFirstCall(app.wsgi_app)
bfc.before_first_call(dummy_once)
bfc.before_first_call(dummy_once)


def test_middleware_dispatcher_wrappers():
    global app
    app.wsgi_app = bfc
    assert_call_count(app)
