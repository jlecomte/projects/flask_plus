from pytest import mark

from flask_gordon.middleware import Dispatcher


@mark.parametrize(
    "mounts, path, expected",
    [
        # fmt: off
        ([], "/", "x"),
        (["/"], "/", "/"),
        #
        (["/", "/api", "/app"], "/",     "/"),
        (["/", "/api", "/app"], "/api",  "/"),
        (["/", "/api", "/app"], "/api/", "/api"),
        (["/", "/api", "/app"], "/app",  "/"),
        (["/", "/api", "/app"], "/app/", "/app"),
        #
        (["/", "/api", "/app"], "/example/one/two",      "/"),
        (["/", "/api", "/app"], "/example/one/two/",     "/"),
        (["/", "/api", "/app"], "/api/example/one/two",  "/api"),
        (["/", "/api", "/app"], "/api/example/one/two/", "/api"),
        (["/", "/api", "/app"], "/app/example/one/two",  "/app"),
        (["/", "/api", "/app"], "/app/example/one/two/", "/app"),
        #
        (["/app", "/api", "/"], "/example/one/two/", "/"),
        (["/app", "/api"],      "/example/one/two/", "x"),
        #
        # fmt: on
    ],
)
def test_middleware_dispatcher(mounts, path, expected):
    class FakeApp:
        def __init__(self, mount):
            self.mount = mount

        def __call__(self, environ, start_response):
            return self.mount

    mounts = {x: FakeApp(x) for x in mounts}
    dispatcher = Dispatcher(FakeApp("x"), mounts)

    retval = dispatcher({"PATH_INFO": path}, None)
    assert retval == expected
