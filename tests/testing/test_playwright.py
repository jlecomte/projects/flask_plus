from flask_gordon.testing.playwright import has_launch_browser


def test_testing_fixture_has_page():
    class MockBrowser:
        def close(self):
            pass

    def no_browser():
        raise NotImplementedError()

    def with_browser():
        return MockBrowser()

    assert has_launch_browser(no_browser) is False
    assert has_launch_browser(with_browser) is True
