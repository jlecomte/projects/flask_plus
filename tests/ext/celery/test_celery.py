from flask import Flask

from flask_gordon.ext import CeleryExt


def test_ext_celery():
    app = Flask(__name__)
    celery = CeleryExt().init_app(app)

    # Assert references in both directions:
    assert app is celery.app()
    assert app.extensions["celery"] is celery
