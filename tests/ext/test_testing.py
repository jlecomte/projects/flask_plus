from flask import Flask, make_response
from pytest import mark

from flask_gordon.ext import FlaskTestingExt


def rule_200_json():
    response = make_response({"json": True}, 200)
    response.headers["Content-Type"] = "application/json; UTF-8"
    return response


def rule_400_json():
    response = make_response({"json": True}, 400)
    response.headers["Content-Type"] = "application/problem+json; UTF-8"
    return response


def rule_200_html():
    response = make_response("<html></html>", 200)
    response.headers["Content-Type"] = "text/html; UTF-8"
    return response


def make_app():
    app = Flask(__name__)
    FlaskTestingExt(app)
    app.config["SECRET_KEY"] = "secret"
    return app


@mark.parametrize(
    "url, rule, status_code, content_type",
    [
        # fmt: off
        ("/", rule_200_html, None, None),
        ("/", rule_200_html, 200, "text/html"),
        ("/", rule_200_html, 200, "*"),
        ("/", rule_200_json, 200, None),
        ("/", rule_200_json, 200, "application/json"),
        ("/", rule_400_json, 400, "application/json"),
        ("/", rule_400_json, 400, "application/problem+json"),
        # fmt: on
    ],
)
def test_ext_testing(url, rule, status_code, content_type):
    app = make_app()
    app.add_url_rule(url, view_func=rule)

    client = app.test_client()

    _ = client.get("/").expect(status_code, content_type=content_type)


#     rv = client.get("/200_html").expect()
#     rv = client.get("/200_html").expect(content_type="text/html")


def test_ext_testing_session():
    app = make_app()
    client = app.test_client()

    # Set some session cookies
    assert len(client.session) == 0

    client.session["one"] = "foobar"
    assert "foobar" == client.session.get("one")
    assert len(client.session) == 1
    del client.session["one"]
    assert len(client.session) == 0

    client.session.set("one", "foobar")
    assert "foobar" == client.session["one"]
    assert len(client.session) == 1

    client.session.clear()
    assert "one" not in client.session
    assert "default" == client.session.get("one", "default")
    assert len(client.session) == 0
    assert len(client.session.items()) == 0
