import argparse

from flask import Flask
from pytest import mark, raises
from torxtools.testtools import disable_outputs

from flask_gordon.ext import ArgParseExt, CeleryExt


def cmdline_test(app, middleware, args, exit_code):
    args = "--cfgfile /dev/null " + args
    try:
        with disable_outputs():
            ArgParseExt().init_app(app, middleware, args.split(" "))
    except FileNotFoundError as err:
        assert exit_code == err.errno, err.errno
    except SystemExit as err:
        try:
            err_code = int(str(err))
        except Exception:
            err_code = None
        assert exit_code == err_code, err_code
    else:
        assert exit_code == 0, "error: did not exit with SystemExit"


@mark.parametrize(
    "middleware, args, exit_code",
    [
        # fmt: off
        ("flask", "--cfgfile /nonexistant", None),
        ("flask", "--listen localhost --port 0", 0),
        ("flask", "--debug", 0),
        ("flask", "--no-debug", 0),
        ("flask", "--ui", 0),
        ("flask", "--workers 0", 2),
        ("flask", "--help", 0),
        ("flask", "--nope", 2),
        #
        ("gunicorn", "--listen localhost --port 0", 0),
        ("gunicorn", "--pidfile", 2),
        ("gunicorn", "--workers -1", 0),
        ("gunicorn", "--workers 0", 0),
        ("gunicorn", "--workers 2", 0),
        ("gunicorn", "--help", 0),
        ("gunicorn", "--nope", 2),
        # fmt: on
    ],
)
def test_flask_ext_argparse(middleware, args, exit_code):
    app = Flask(__name__)
    if exit_code is None:
        with raises(argparse.ArgumentTypeError):
            cmdline_test(app, middleware, args, exit_code)
    else:
        cmdline_test(app, middleware, args, exit_code)


@mark.parametrize(
    "args, exit_code",
    [
        # fmt: off
        ("", 0),
        ("--config /dev/null", 0),
        # fmt: on
    ],
)
def test_celery_ext_argparse(args, exit_code):
    app = Flask(__name__)
    celery = CeleryExt().init_app(app)
    cmdline_test(celery, "celery", args, exit_code)
