# ==============================================================================
# Python3.x
# ==============================================================================
#.PHONY: check test tests pytest
3.8 3.9 3.10 3.11 3.12: FORCE
	$$(pyenv which python$@) -mvenv .venv/$@
	.venv/$@/bin/pip$@ install -r requirements-dev.txt
	PYTHON=.venv/$@/bin/python$@ make check

3.x: 3.8 3.9 3.10 3.11 3.12
